# Microservices in Oracle Visual Builder Studio

## Before you begin for test

 This tutorial shows you how to create a project in Oracle Visual Builder Studio (VB Studio) and import a Node.js microservice application's source code files from a Git repository.

### Background
Oracle Visual Builder Studio (VB Studio) is a DevOps and lifecycle management tool, and provides the infrastructure to help you build and deploy apps.

A VB Studio project is a collection of Git repositories, branch merge requests, wikis, issues, deployment configurations, and builds.

### What Do You Need?
* A web browser
* Your Oracle Cloud account credentials
* Access to a VB Studio instance
* DEVELOPER_ADMINISTRATOR identity domain role assigned to you

### 1) VB Studio Console

* In a web browser's address bar, enter https://cloud.oracle.com/sign-in.
* In Cloud Account Name, enter your Oracle Cloud account name and click Next.
* On the Single Sign-On page, click Continue.
* On the Oracle Cloud Account Sign In page, enter your Oracle Cloud username and password, and then click Sign In.
* In the upper-left corner, click Navigation Menu Menu icon and select OCI Classic Services.
* Under Platform Services, select Developer.

### 1.1) Instance creation

Click in the "Create Instance" button and provide the information required. The process would be simple and you have to wait to get all resources provisioned. Once created the instance:

* In the Instances tab, click Manage this instanceAction menu and select Access Service Instance.

### 1.2) Configure Visual Studio to use OCI resources

Under your organization -> OCI account, set the next values:

![OCI](assets/p/p1.png)

* ** Tenancy OCID** : Found clicking in the profile icon, under **Tenancy details**
* ** User OCID**: Found clicking in the profile icon, under **User settings**
* **Private Key**: Found clicking in the profile icon, under **User settings** -> API Keys in the left menu
* **Finger Print**:Found clicking in the profile icon, under **User settings** -> API Keys in the left menu
* **Compartment OCID**: Go to the compartments list, select your compartment and click on it to reveal the details.
* **Storage Namespace**: Found clicking in the profile icon, under **Tenancy details**

### 1.3) Build executors templates

In order to build the images, you need to configure the build executor template to use; for that, go to "organization" and click on Build executor Templates and push the "Create Template" button.

![OCI](assets/p/p2.png)

Provide the name of the executor and the selected base image

![OCI](assets/p/p3.png)

Under the software section, select "Docker" and "NodeJS"

![OCI](assets/p/p4.png)
![OCI](assets/p/p5.png)
![OCI](assets/p/p6.png)

### 1.4) Build executors

Once the template is created, you can create multiple execlutors to add to the pool. To do that, go to "Organization" -> VM Build Executors, and click "Create":

![OCI](assets/p/p7.png)

Provide the quantity of the VM to add to the executors pool, the recently created template, the region and the desired shape.

![OCI](assets/p/p8.png)

The executor will be created and you can confirm the status in the VM listing

![OCI](assets/p/p9.png)

You can optimize the resources sleepingn down the executors after certain idle time. To achieve that, click in the "Sleep timeout" button and set the desired idle time, after which the VM will be set in the sleeping mode.

![OCI](assets/p/p10.png)

### 2) Create a Project
* On the Organization page, click + Create.

![OCI](assets/projects/a1.png)

* On the Project Details page of the New Project wizard, provide the name and description.
  
![OCI](assets/projects/a2.png)

* Create an initial repository, which will create a README to begin with

![OCI](assets/projects/a3.png)

* In te latest step enter these values and click Next.
  
> Import existing repository :https://github.com/oraclevbstudio/my_nodejs_app.git  
  
![OCI](assets/projects/a4.png)

Wait for the project's provisioning to complete. After provisioning completes, the Project Home page opens.


![OCI](assets/projects/a5.png)

In the Repositories tab, click the Git repository's name to view these imported files:

![OCI](assets/projects/a6.png)

  > main.js: The main Node.js file with these functions:
  >   A get function that outputs a static Hello World message and a false error message
  >   A post function that adds two numbers passed as input at runtime
  
The app uses Node.js modules express and bodyParser to create the REST service.

  > package.json: The JSON file that defines the app's name as NodeJSMicro, and bodyParser and express as its dependencies.
  > README.md: The Readme wiki file

The project's Project Owner role is also granted to you. In the navigation menu, click Project Home and then click the Team tab to verify the Project Owner label next to your name.

### 3) Build and push microservice Docker Image to OCI Registry

OCIR is an Oracle-managed registry that enables you to store, share, and manage development artifacts like Docker images. You can use OCIR as a private Docker registry for internal and public use, pushing and pulling Docker images to and from the Registry using the Docker V2 API and the standard Docker command-line interface (CLI).

> You will need the OCI Region (into the tenancy details).
> Your tenancy name also will be needed
> Your OCI username and users auth token

### 3.1) Add the Dockerfile to the repository

* On the Organization page, click the Projects tab.
* Click the My Nodejs App project.
* In the left navigation menu, click Git.
* From the Repositories list, select my-nodejs-app.git.
* Click + File.

![OCI](assets/repository/git_populated_repo.png)

* In File Name, enter Dockerfile.
* In the content area, copy and paste this code.

```
FROM node:15
ADD main.js ./
ADD package.json ./
RUN npm install
EXPOSE 80
CMD [ "npm", "start" ]
```

* Click commit


![OCI](assets/repository/git_addfile.png)

### 3.2) Configure build job

* In the left navigation menu, click Builds.
* In the Jobs tab, click + Create Job.
* In the New Job dialog box, enter these values and click Create.
  * Job Name: build-nodejs-docker
  * Description: Job to create a Docker image of a Node.js app and push it to OCIR
  * Template: Node.js and Docker

![OCI](assets/jobs/new_job_dialog.png)

* In the Configure Configure tab, click the Git tab.
* From Add Git, select Git.

![OCI](assets/jobs/configure_job_sourcecontrol.png)

* Click the Steps tab. From Add Step, select Docker, and then select Docker login.

![OCI](assets/jobs/configure_job_addstep.png)

* In Registry Host, enter this value: iad.ocir.io
* In Registry Username, enter this value: tenancy-namespace/username
* In Password, enter this value: The token generated under your account
* From Add Step, select Docker, and then select Docker build.
* In Image Name, enter this value tenancy-namespace/registry-name/node_service

![OCI](assets/jobs/configure_job_dockerbuild.png)

* From Add Step, select Docker, and then select Docker push.

>No configuration is required in the Docker push section. It picks the required values from the Docker login and the Docker build sections.

* Scroll up and click Save.
* On the job's details page, click Build Now.
* Wait for the build to complete. If it's VM's first build, it'll take some time.
* After the build is complete, click Build Log Build Log icon. You'll see a SUCCESSFUL message at the end of the build log.
* Open the Oracle Cloud Console.
* In the navigation menu, select Solutions and Platforms. Under Developer Services, select Container Registry (OCIR).
* Verify the registry image pushed to OCIR.

### 3.3) Run the app

* On your computer, open the Docker terminal.
* Run this command to log into OCIR:

```bash
$ docker login iad.ocir.io
```

When prompted, enter the user name and the auth token as the password. The password isn't visible when you type it. Press Enter when you're done.

```bash
Your username:
```

```bash
Your password:
```

* Run this command to pull the Docker image you pushed to OCIR from the build job:

```bash
$ docker pull iad.ocir.io/myaccount/ociuser/my_nodejs_image
```

* Wait for the image to download.

```bash
Status: Downloaded newer image for iad.ocir.io/myaccount/ociuser/my_nodejs_image:latest
iad.ocir.io/myaccount/ociuser/my_nodejs_image:latest
```

* Run this command to publish the image to a local container:
  
```bash
$ docker run -p 4000:80 iad.ocir.io/myaccount/ociuser/my_nodejs_image
```

> The above command runs the container and opens its port 80.

* In a web browser, enter http://localhost:4000 to open the application.

* To test the /add function of main.js, use curl.

```bash
$ curl -s -d "num1=11&num2=22" -X POST http://localhost:4000/add
{"error":false,"message":"success","data":33}
```

### 3.4) Run tests

In te job configuration, go to steps and add new step at the beginig:

* Under common build tools, select Unix Shell and add the next commands

```bash
npm config set registry https://devops-ladcloudmx.developer.ocp.oraclecloud.com/profile/devops-ladcloudmx/s/devops-ladcloudmx_microservices_34866/npm/
_auth=b3NjYXIudmVudHVyYUBvcmFjbGUuY29tOjBzY2FyLjIwMDkuMXZhTg== email=oscar.ventura@oracle.com

npm install -g mocha mocha-junit-reporter
mocha test --reporter mocha-junit-reporter 

npm version patch
npm publish
```

Save the job and run it. After running it, go to Test Results.

![OCI](assets/jobs/a.png)